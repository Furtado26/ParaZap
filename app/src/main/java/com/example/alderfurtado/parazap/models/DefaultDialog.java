package com.example.alderfurtado.parazap.models;

import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alder.furtado on 16/05/2018.
 */

public class DefaultDialog implements IDialog {

    private String id;
    private String dialogPhoto;
    private String dialogName;
    private ArrayList<Author> users;
    private IMessage iMessage;

    public void setId(String id) {
        this.id = id;
    }

    public void setDialogPhoto(String dialogPhoto) {
        this.dialogPhoto = dialogPhoto;
    }

    public void setDialogName(String dialogName) {
        this.dialogName = dialogName;
    }

    public void setUsers(ArrayList<Author> users) {
        this.users = users;
    }

    public IMessage getiMessage() {
        return iMessage;
    }

    public void setiMessage(IMessage iMessage) {
        this.iMessage = iMessage;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getDialogPhoto() {
        return dialogPhoto;
    }

    @Override
    public String getDialogName() {
        return dialogName;
    }

    @Override
    public ArrayList<Author> getUsers() {
        return users;
    }

    @Override
    public IMessage getLastMessage() {
        return iMessage;
    }

    @Override
    public void setLastMessage(IMessage message) {
        this.iMessage = message;
    }

    @Override
    public int getUnreadCount() {
        return 0;
    }
}
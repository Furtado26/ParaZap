package com.example.alderfurtado.parazap;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.alderfurtado.parazap.models.Author;
import com.example.alderfurtado.parazap.models.DefaultDialog;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

import java.util.ArrayList;
import java.util.Date;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private Author author;
    private DefaultDialog defaultDialog;
    private DefaultDialog defaultDialog2;
    private DefaultDialog defaultDialog3;

    private ArrayList<Author> authors = new ArrayList<>();
    private DialogsList dialogsList ;

    InputMethodManager imm;


    ImageButton search_button;
    EditText search_contact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        search_button = findViewById(R.id.search_button);
        search_contact = findViewById(R.id.search_bar);

        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_contact.setFocusable(true);
                search_contact.setFocusableInTouchMode(true);
                search_contact.requestFocus();
                search_contact.setHint("Pesquisar...");

                //TODO on beck press Set hint and hint color

                imm.showSoftInput(search_contact, InputMethodManager.SHOW_IMPLICIT);
            }

            public boolean dispatchKeyEventPreIme(KeyEvent event){
                if(event != null){
                    int keyCode = event.getKeyCode();
                    if(keyCode == KeyEvent.KEYCODE_BACK){
                        // hide search suggestions UI
                    }
                }
                return true;
            }


        });


//fazer busca
        search_contact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                search(charSequence.toString().toLowerCase().trim());
                Log.i("aperto durante","sim");
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        for(int i = 0;i < 3;i++) {
            author = new Author();
            author.setId("" + i);
            author.setName("teste" + i);
            author.setAvatar("avatar" + i);
            authors.add(author);
        }

        IMessage iMessage = new IMessage() {
            @Override
            public String getId() {
                return "2";
            }

            @Override
            public String getText() {
                return "Vão trabalhar";
            }

            @Override
            public IUser getUser() {
                return author;
            }

            @Override
            public Date getCreatedAt() {
                Date date = new Date();
                Log.i("Hora de hoje",date.toString());
                return  date;
            }
        };

        dialogsList = (DialogsList)findViewById(R.id.dialogsList) ;

        defaultDialog = new DefaultDialog();
        defaultDialog.setId("1");
        defaultDialog.setDialogName("Grupo dos estagiários");
        defaultDialog.setDialogPhoto("http://i42.tinypic.com/6507m0");
        defaultDialog.setUsers(authors);
        defaultDialog.setiMessage(iMessage);

        defaultDialog2 = new DefaultDialog();
        defaultDialog2.setId("2");
        defaultDialog2.setDialogName("Grupo da Presidência");
        defaultDialog2.setDialogPhoto("http://i42.tinypic.com/6507m0");
        defaultDialog2.setUsers(authors);
        defaultDialog2.setiMessage(iMessage);

        defaultDialog3 = new DefaultDialog();
        defaultDialog3.setId("3");
        defaultDialog3.setDialogName("Grupo das faltas");
        defaultDialog3.setDialogPhoto("http://i42.tinypic.com/6507m0");
        defaultDialog3.setUsers(authors);
        defaultDialog3.setiMessage(iMessage);

        DialogsListAdapter dialogsListAdapter = new DialogsListAdapter<>(R.layout.item_dialog, new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                //If you using another library - write here your way to load image
                Glide.with(HomeActivity.this).load(url).into(imageView);
            }
        });

        dialogsListAdapter.addItem(defaultDialog);
        dialogsListAdapter.addItem(defaultDialog2);
        dialogsListAdapter.addItem(defaultDialog3);
        dialogsList.setAdapter(dialogsListAdapter);


        dialogsListAdapter.setOnDialogClickListener(new DialogsListAdapter.OnDialogClickListener() {
            @Override
            public void onDialogClick(IDialog dialog) {
                Intent intent = new Intent(HomeActivity.this,ConversaActivity.class);
//                intent.putExtras("dialog", (Serializable) dialog);
                startActivity(intent);

            }
        });
    }



    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

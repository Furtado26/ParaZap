package com.example.alderfurtado.parazap;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.alderfurtado.parazap.models.Messagem;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IUser;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.Date;

public class ConversaActivity extends AppCompatActivity {

    MessageInput messageInput;
//    DefaultDialog defaultDialog;
    ImageLoader imageLoader;
    IUser iUser = new IUser() {
        @Override
        public String getId() {
            return "id";
        }

        @Override
        public String getName() {
            return "Alder";
        }

        @Override
        public String getAvatar() {
            return "";
        }
    };
    MessagesList messagesList;
    Messagem messagem = new Messagem();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        messageInput = (MessageInput) findViewById(R.id.input);

        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {

            }
        };

        final MessagesListAdapter<Messagem> adapter = new MessagesListAdapter<>("1",imageLoader);
        messagesList = (MessagesList) findViewById(R.id.messagesList) ;

        messagesList.setAdapter(adapter);


        messagem.setId("1");
        messagem.setiUser(iUser);
        Date date = new Date();
        messagem.setDate(date);

        messageInput.setInputListener(new MessageInput.InputListener() {
            @Override
            public boolean onSubmit(CharSequence input) {
                messagem.setText(input.toString());
                adapter.addToStart(messagem,true);
                return true;
            }
        });

//        messageInput.setInputListener(new MessageInput.InputListener() {
//            @Override
//            public boolean onSubmit(CharSequence input) {
//                return false;
//            }
//        });

//        Bundle bundle = getIntent().getExtras();
//        if(bundle != null){
//            defaultDialog = (DefaultDialog) bundle.get("dialog");
//        }
//
//        imageLoader = new ImageLoader() {
//            @Override
//            public void loadImage(ImageView imageView, String url) {
//                Glide.with(getBaseContext()).load(url).into(imageView);
//            }
//        };
//
//        Log.i("Dialog",defaultDialog.getDialogName());
//        MessagesListAdapter<IMessage> adapter = new MessagesListAdapter<>("Olá", imageLoader);
//        messagesList.setAdapter(adapter);
    }
}
